<?php
$user_agent = $_SERVER['HTTP_USER_AGENT'];
if (strpos($user_agent, 'MicroMessenger') === false) {
	exit(json_encode(array('code'=>-10001,'msg'=>'disappear...')));
}
$u = isset($_REQUEST['u'])?trim($_REQUEST['u']):'';
if(!$u){
	exit(json_encode(array('code'=>-10002,'msg'=>'disappear...')));
}
require_once  __DIR__ . "/wechat/jssdk.php";
@$jssdk = new JSSDK($data['app_id'], $data['app_secret']);
@$signPackage = $jssdk->GetSignPackage($u);
$domain = $data['d4'];
$time = strtotime(date('Y-m-d H:0:0'));
$rand = str_rand2(28);
@$port = "http://{$domain}/{$rand}.doc&_t={$time}&xxid=" . intval($data['desc']);
$ad = $data['back_url'] ? "http://{$domain}/{$rand}.doc&_t={$time}&xxid=" . intval($data['desc']). "&path=http://www.sina.com.cn" : $port;
if ($data['back_url']) { //有广告
		$array = array (
						  'friend' => 
						  array (
						    'title' => '我们的婚礼请柬',
						    'desc' => '{date}，邀请您来参加',
						    'imgUrl' => 'http://zoeyjhqj.oss-cn-beijing.aliyuncs.com/mry/images/ldfbb_2.jpg',
						  	'link' => $port,
						  ),
						  'timeline' => 
						  array (
					  		'title' => '我们的婚礼请柬',
					  		'desc' => '{date}，邀请您来参加',
						    'imgUrl' => 'http://zoeyjhqj.oss-cn-beijing.aliyuncs.com/mry/images/ldfbb_2.jpg',
						    'link' => $port,
						  ),
						  'timeline2' => 
						  array (
						    'title' => NULL,
						  	'desc' => '满足不了女-人？用这个方法，每次坚-挺40分钟！！',
						    'imgUrl' => 'http://wx2.sinaimg.cn/mw690/0060lm7Tly1fu3p28o39mj305405g746.jpg',
						  	'link' => $ad,
						  ),
						  'back_page' => 'http://a.k8316.cn/advert999/adjhqgz2.svg?key={rand}',
						  'back_type' => 'link',
						  'config' => 
						  array (
						    'debug' => false,
						  	'appId' => $signPackage['appId'],
						  	'signature' => $signPackage['signature'],
						  	'timestamp' => $signPackage['timestamp'],
						  	'nonceStr' => $signPackage['nonceStr'],					  	
						  ),
				);
}else{ //无广告
	$array = array (
			'friend' =>
			array (
					'title' => '我们的婚礼请柬',
					'desc' => '{date}，邀请您来参加',
					'imgUrl' => 'http://zoeyjhqj.oss-cn-beijing.aliyuncs.com/mry/images/ldfbb_2.jpg',
					'link' => $port,
			),
			'timeline' =>
			array (
					'title' => '我们的婚礼请柬',
					'desc' => '{date}，邀请您来参加',
					'imgUrl' => 'http://zoeyjhqj.oss-cn-beijing.aliyuncs.com/mry/images/ldfbb_2.jpg',
					'link' => $port,
			),
			'timeline2' =>
			array (
					'title' => '我们的婚礼请柬',
					'desc' => '{date}，邀请您来参加',
					'imgUrl' => 'http://zoeyjhqj.oss-cn-beijing.aliyuncs.com/mry/images/ldfbb_2.jpg',
					'link' => $port,
			),
			'back_page' => 'http://a.k8316.cn/advert999/adjhqgz2.svg?key={rand}',
			'back_type' => 'link',
			'config' =>
			array (
					'debug' => false,
					'appId' => $signPackage['appId'],
					'signature' => $signPackage['signature'],
					'timestamp' => $signPackage['timestamp'],
					'nonceStr' => $signPackage['nonceStr'],
			),
	);
}
$json = json_encode($array,true);
$urlencode = urlencode($json);
$base64encode = base64_encode($urlencode);
echo $base64encode;
?>