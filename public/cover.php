<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0023)http://www.npnslab.org/ -->
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link href="./cover_files/style.css" rel="stylesheet" type="text/css">


<title>北京市纳米光电子重点实验室</title> 
<meta name="keywords" content="纳米光电子，纳米材料，光电检测，纳米测试，测试服务，开放实验室">
<meta name="description" content="北京市纳米光电子重点实验室，开放实验室，测试服务平台">
<script type="text/javascript" src="./cover_files/jquery-1.8.2.min.js"></script>

<script type="text/javascript" src="./cover_files/slider.js"></script> 

<script type="text/javascript">

	$(function(){

		$('#demo01').flexslider({

			animation: "slide",

			direction:"horizontal",

			easing:"swing"

		});

		});

</script>

</head>



<body>

<div class="all">

<div class="logobox">

	<div class="logo">

    	<a href="http://www.npnslab.org/#"><img src="./cover_files/index_02.jpg" width="1000" height="86"></a>

    </div>

</div>

<div class="navbox">

	<div class="nav">

    	<ul>

			<li><a class="currentA" href="http://www.npnslab.org/">首页</a></li>

			
			
			<li><a href="http://www.npnslab.org/abouts.html">概况</a></li>
			
			<li><a href="http://www.npnslab.org/tplbs.html">仪器设备</a></li>
			
			<li><a href="http://www.npnslab.org/services.html">技术服务</a></li>
			
			<li><a href="http://www.npnslab.org/newss.html">测试交流</a></li>
			
			<li><a href="http://www.npnslab.org/lyzxs.html">留言咨询</a></li>
			
			<li><a href="http://www.npnslab.org/csyys.html">测试预约</a></li>
			
            <li><a href="http://www.npnslab.org/lxwms.html">联系我们</a></li>
        </ul>

    </div>

</div>





<!-- <script>



	$(function(){



		$(".btn").click(function(){



			var sub=$(".btn").val();



			//alert(sub);



			var name=$(".name").val();



			var password=$(".password").val();



			$.post("admins.html",{"name":name,"password":password,"sub":sub},function(data){



				if(0==data){



					var rs ="登入失败！"



					$(".alert").html(rs);



				}



			});



		});



	});



</script> -->



<div id="demo01" class="flexslider">



	



<div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 1200%; transition-duration: 0s; transform: translate3d(-1000px, 0px, 0px);"><li class="clone" style="width: 1000px; float: left; display: block;"><div class="img"><img src="./cover_files/4dc1846ef9e13a469ec4148be7991c18.jpg" height="377" width="1000" alt=""></div></li>



	<li class="flex-active-slide" style="width: 1000px; float: left; display: block;"><div class="img"><img src="./cover_files/60e2bb224c0436b7c0ee936ebd469b85.jpg" height="377" width="1000" alt=""></div></li><li style="width: 1000px; float: left; display: block;" class=""><div class="img"><img src="./cover_files/663f6b13301bd75fbfffb1a375a3ce6b.jpg" height="377" width="1000" alt=""></div></li><li style="width: 1000px; float: left; display: block;" class=""><div class="img"><img src="./cover_files/2a06e7a735cc2b175550291a947517a6.jpg" height="377" width="1000" alt=""></div></li><li style="width: 1000px; float: left; display: block;" class=""><div class="img"><img src="./cover_files/4dc1846ef9e13a469ec4148be7991c18.jpg" height="377" width="1000" alt=""></div></li>


	<li class="clone" style="width: 1000px; float: left; display: block;"><div class="img"><img src="./cover_files/60e2bb224c0436b7c0ee936ebd469b85.jpg" height="377" width="1000" alt=""></div></li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a class="flex-active">1</a></li><li><a class="">2</a></li><li><a class="">3</a></li><li><a class="">4</a></li></ol><ul class="flex-direction-nav"><li><a class="flex-prev" href="http://www.npnslab.org/#">Previous</a></li><li><a class="flex-next" href="http://www.npnslab.org/#">Next</a></li></ul></div><!--flexslider end-->



<div class="middlebox">



	<div class="middle">



    	<div class="shangbu">



        	<div class="kjdtbox">



        	<div class="kjtitle">



            	<div class="kjtb"><img src="./cover_files/index_14.jpg" width="25" height="25"></div>



                <div class="kjbt"><a href="http://www.npnslab.org/kejis.html" style="float:left;">科技动态</a><span style="float:left;color:#06F;padding-left:100px"><a href="http://www.npnslab.org/kejis.html">更多&gt;&gt;</a></span></div>



            </div>



            <div class="kjnr">



				<div class="kjlinkA"><a href="http://www.npnslab.org/kejis17.html">全球首台狭缝分光拉曼样机在中国石化问世</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/kejis16.html">第十七届全国等离子体科学技术会议</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/kejis15.html">International Workshop of Advanced Image ...</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/kejis14.html">激光诱导石墨烯制备超级的电子产品</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/kejis13.html">2015年Light Conference－青年科学家论坛</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/kejis12.html">碳纳米管创造人工细胞膜通道 有望实现精确治疗</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/kejis11.html">致力石墨烯走向产业化 ——记华侨大学教授陈国华</a></div>


            </div>



        </div>



        	<div class="kjdtbox">



        	<div class="kjtitle">



            	<div class="kjtb"><img src="./cover_files/index_14.jpg" width="25" height="25"></div>



                <div class="kjbt"><a href="http://www.npnslab.org/newss.html" style="float:left;">测试交流</a><span style="float:left;color:#06F;padding-left:100px"><a href="http://www.npnslab.org/newss.html">更多&gt;&gt;</a></span></div>



            </div>



            <div class="kjnr">



				<div class="kjlinkA"><a href="http://www.npnslab.org/newsss69.html">PECVD镀膜</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/newsss67.html">移动针尖进行刻写的办法</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/newsss16.html">表面形貌观察</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/newsss17.html">纳米材料分析</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/newsss19.html">晶体缺陷分析</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/newsss20.html">显微结构的分析</a></div><div class="kjlinkA"><a href="http://www.npnslab.org/newsss21.html">扫描电子显微镜的优势</a></div>


            </div>



        </div>



            <div class="kjdtboxA">



                <div class="kjtitleA">



                    <div class="kjtbA"><img src="./cover_files/index_14.jpg" width="25" height="25"></div>



					


					<div class="kjbtA"><a href="http://www.npnslab.org/#">预约登陆</a></div>


                    



                </div>



                <div id="login" class="clearfix">



				<!--  -->



					<form method="post" action="http://www.npnslab.org/indexs.html">



                      <div class="message clearfix">



                            <label>用户名：</label>



                            <input type="text" name="name" class="name" value="">



                        </div>



                      <div class="message clearfix">



                            <label>密码：</label>



                            <input type="password" name="password" class="password" value="">



                        </div>



                        <div id="btn">



                            <input type="submit" name="sub" class="btn" value="登录">



                            <input type="submit" name="sub" class="btnA" value="注册">



                      </div>



				   <input type="hidden" name="__hash__" value="6666cd76f96956469e7be39d750cc7d9_5d954b54c6fcd0fde5cc9621415f326b"></form>



				



                </div>







            </div>



        </div>



    	<div class="xiabu">



        	<div class="contact">



            	<div class="C_title">联系我们</div>



                <div class="C_nr">



                	<div class="C_tel">联系电话：010-68902332-607</div>



                    <div class="C_tel">QQ：2729894741</div>



                    <div class="C_tel">地址：北京市西三环北路105号</div>



                    <div class="C_tel">邮编：100048</div>



                    <div class="C_tel">邮箱：npnslab@126.com</div>



                </div>



            </div>



            <div class="yqbox">



            	<div class="yqtitle">仪器设备</div>

                

                

                <div class="mxcon">

                <div class="mxconz">

                <ul><div id="demo" style="overflow:hidden;width:864px;height:151px;margin:0px 0px 0px 0px;padding:0px; overflow:hidden;" align="center">

                <table border="0" align="center" cellpadding="1" cellspacing="1" cellspace="0">

                  <tbody><tr><td valign="top" id="marquePic1">

                    <table width="100%" border="0" cellspacing="10">

                    <tbody><tr>

                  <td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels19.html"><img src="./cover_files/bc4299e314dc735594dd8a8f0201cfb4.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels19.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">显微拉曼光谱仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels34.html"><img src="./cover_files/a608ac5489f559163bd968f4b84976ce.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels34.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">表面等离子体暗场光谱系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels33.html"><img src="./cover_files/b918c2c10a4e26cbdc0b53ef96715f8d.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels33.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">全谱吸收光谱仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels36.html"><img src="./cover_files/a03a383d9277e54242f65ca110ab3e7f.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels36.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;"> RFS100/S付立叶拉曼光谱仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels37.html"><img src="./cover_files/4a636f63abaffe857fb2ff477dee6808.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels37.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">旋光拉曼仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels38.html"><img src="./cover_files/493c61ad4661acd188e35da278856912.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels38.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">针尖增强拉曼光谱系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels39.html"><img src="./cover_files/641fbb8b065806a7cea251068dfdd81c.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels39.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">近场光学微区增强拉曼光谱系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels41.html"><img src="./cover_files/8df520522fb96d2650cbb167d9dcfc14.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels41.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">紫外分光光度计</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels26.html"><img src="./cover_files/c37c674143fb6c3802c2debadd65fafd.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels26.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">原子力显微镜</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels20.html"><img src="./cover_files/a4dd308e4f2dec54f1e6c6dda41e9b84.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels20.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">扫描电子显微镜</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels18.html"><img src="./cover_files/04f2c0ac2468dd62e32d7888574ee6e1.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels18.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">蔡司显微镜</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels14.html"><img src="./cover_files/663002eaa5fa6f2c5ad191131481576e.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels14.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">拉曼光谱仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels45.html"><img src="./cover_files/c97b07ac1e4158aa41159057de7f6503.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels45.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">透射电子显微镜</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels44.html"><img src="./cover_files/3ede3d8950637eaafbb07089470a924e.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels44.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">微纳测试系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels43.html"><img src="./cover_files/56bea6190286ae35f5cd90059ab97d6c.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels43.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">光电测试系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels42.html"><img src="./cover_files/66d8213a348f3c4da2291450142fe21c.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels42.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">电阻温度测试系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels40.html"><img src="./cover_files/9f4bf3aa600a1a63a2e4634449ec0b3f.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels40.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">纳米碳材料制备系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels35.html"><img src="./cover_files/7c0a25309fa5f93aa83a3152ff4120f7.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels35.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">等离子体增强化学气相沉积系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels32.html"><img src="./cover_files/c19f6abf9414954fa0e5132a87930ba1.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels32.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">离子溅射仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels31.html"><img src="./cover_files/e1b9baee116fa34e53bb61a8fc1bcf8f.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels31.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">热蒸发真空蒸镀系统（PLA）</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels30.html"><img src="./cover_files/aa2f33c612aab39490a556406f495b97.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels30.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">532nm激光器</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels29.html"><img src="./cover_files/4eda3c0d51f3f7037b4e66eb35f018e5.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels29.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">大功率红外1064nm激光器</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels28.html"><img src="./cover_files/c6f31addc06045a6ee0bac3f4439f66c.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels28.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">准分子激光器</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels24.html"><img src="./cover_files/ed1b65c896cd5982e252090f67da95a6.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels24.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">太阳能效率测试仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels22.html"><img src="./cover_files/821727bbe220d21a0fd0d97dca270d29.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels22.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">激光脉冲沉积系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels21.html"><img src="./cover_files/c4ed27dff012011234e6d27969e6e18c.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels21.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">磁控溅射系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels15.html"><img src="./cover_files/89732ecdc6a305fe8afeb857c0e9ae15.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels15.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">X射线衍射仪</a></td>
                    </tr></tbody></table>

                  </td><td id="marquePic2" valign="top">

                    <table width="100%" border="0" cellspacing="10">

                    <tbody><tr>

                  <td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels19.html"><img src="./cover_files/bc4299e314dc735594dd8a8f0201cfb4.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels19.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">显微拉曼光谱仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels34.html"><img src="./cover_files/a608ac5489f559163bd968f4b84976ce.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels34.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">表面等离子体暗场光谱系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels33.html"><img src="./cover_files/b918c2c10a4e26cbdc0b53ef96715f8d.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels33.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">全谱吸收光谱仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels36.html"><img src="./cover_files/a03a383d9277e54242f65ca110ab3e7f.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels36.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;"> RFS100/S付立叶拉曼光谱仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels37.html"><img src="./cover_files/4a636f63abaffe857fb2ff477dee6808.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels37.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">旋光拉曼仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels38.html"><img src="./cover_files/493c61ad4661acd188e35da278856912.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels38.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">针尖增强拉曼光谱系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels39.html"><img src="./cover_files/641fbb8b065806a7cea251068dfdd81c.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels39.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">近场光学微区增强拉曼光谱系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels41.html"><img src="./cover_files/8df520522fb96d2650cbb167d9dcfc14.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels41.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">紫外分光光度计</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels26.html"><img src="./cover_files/c37c674143fb6c3802c2debadd65fafd.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels26.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">原子力显微镜</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels20.html"><img src="./cover_files/a4dd308e4f2dec54f1e6c6dda41e9b84.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels20.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">扫描电子显微镜</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels18.html"><img src="./cover_files/04f2c0ac2468dd62e32d7888574ee6e1.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels18.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">蔡司显微镜</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels14.html"><img src="./cover_files/663002eaa5fa6f2c5ad191131481576e.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels14.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">拉曼光谱仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels45.html"><img src="./cover_files/c97b07ac1e4158aa41159057de7f6503.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels45.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">透射电子显微镜</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels44.html"><img src="./cover_files/3ede3d8950637eaafbb07089470a924e.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels44.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">微纳测试系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels43.html"><img src="./cover_files/56bea6190286ae35f5cd90059ab97d6c.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels43.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">光电测试系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels42.html"><img src="./cover_files/66d8213a348f3c4da2291450142fe21c.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels42.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">电阻温度测试系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels40.html"><img src="./cover_files/9f4bf3aa600a1a63a2e4634449ec0b3f.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels40.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">纳米碳材料制备系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels35.html"><img src="./cover_files/7c0a25309fa5f93aa83a3152ff4120f7.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels35.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">等离子体增强化学气相沉积系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels32.html"><img src="./cover_files/c19f6abf9414954fa0e5132a87930ba1.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels32.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">离子溅射仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels31.html"><img src="./cover_files/e1b9baee116fa34e53bb61a8fc1bcf8f.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels31.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">热蒸发真空蒸镀系统（PLA）</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels30.html"><img src="./cover_files/aa2f33c612aab39490a556406f495b97.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels30.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">532nm激光器</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels29.html"><img src="./cover_files/4eda3c0d51f3f7037b4e66eb35f018e5.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels29.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">大功率红外1064nm激光器</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels28.html"><img src="./cover_files/c6f31addc06045a6ee0bac3f4439f66c.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels28.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">准分子激光器</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels24.html"><img src="./cover_files/ed1b65c896cd5982e252090f67da95a6.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels24.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">太阳能效率测试仪</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels22.html"><img src="./cover_files/821727bbe220d21a0fd0d97dca270d29.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels22.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">激光脉冲沉积系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels21.html"><img src="./cover_files/c4ed27dff012011234e6d27969e6e18c.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels21.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">磁控溅射系统</a></td><td align="center" valign="top" style="width:135px;height:141px; overflow:hidden;"><a href="http://www.npnslab.org/tplbsels15.html"><img src="./cover_files/89732ecdc6a305fe8afeb857c0e9ae15.jpg" width="135" height="106" style="border:3px solid #849ece;"></a><br><a href="http://www.npnslab.org/tplbsels15.html" style="width:135px; height:35px; line-height:35px; color:#666;text-align:center; overflow:hidden;">X射线衍射仪</a></td>
                    </tr></tbody></table>

                  </td></tr>

                </tbody></table>

                </div>

                </ul>

                </div>

                </div>

  

			<!--

                <div class="yqtpbox">



					<div class="yqtp">



                    	<div class="yqimg"><a href="yiqis1.html"><img src="1" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis1.html">1</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqisX.html"><img src="X" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqisX.html">X</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis2.html"><img src="2" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis2.html">2</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis�.html"><img src="�" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis�.html">�</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqisX.html"><img src="X" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqisX.html">X</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis�.html"><img src="�" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis�.html">�</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis0.html"><img src="0" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis0.html">0</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis�.html"><img src="�" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis�.html">�</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis�.html"><img src="�" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis�.html">�</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis
.html"><img src="
" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis
.html">
</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis<.html"><img src="<" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis<.html"><</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis2.html"><img src="2" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis2.html">2</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis0.html"><img src="0" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis0.html">0</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis�.html"><img src="�" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis�.html">�</a></div>



                    </div><div class="yqtp">



                    	<div class="yqimg"><a href="yiqis0.html"><img src="0" width="135" height="106" /></a></div>



                        <div class="yqwz"><a href="yiqis0.html">0</a></div>



                    </div>


                </div>

                -->



            </div>



        </div>



    </div>



</div>

<script type="text/javascript">

var speed=50 

marquePic2.innerHTML=marquePic1.innerHTML 

function Marquee(){ 

if(demo.scrollLeft>=marquePic1.scrollWidth){ 

demo.scrollLeft=0 

}else{ 

demo.scrollLeft++ 

} 

} 

var MyMar=setInterval(Marquee,speed) 

demo.onmouseover=function() {clearInterval(MyMar)} 

demo.onmouseout=function() {MyMar=setInterval(Marquee,speed)} 

</script>





<div class="bottombox">

	<div class="bottom">

    	<div class="weixin">

            <div class="banq">北京市纳米电子重点实验室版权所有 ICP备案号：京ICP备14036819号<br>

地址：北京市西三环北路105号  邮编：100048  电话：010-68902332-607<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1254613194'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s11.cnzz.com/z_stat.php%3Fid%3D1254613194%26show%3Dpic1' type='text/javascript'%3E%3C/script%3E"));</script><span id="cnzz_stat_icon_1254613194"><a href="https://www.cnzz.com/stat/website.php?web_id=1254613194" target="_blank" title="站长统计"><img border="0" hspace="0" vspace="0" src="./cover_files/pic1.gif"></a></span><script src="./cover_files/z_stat.php" type="text/javascript"></script><script src="./cover_files/core.php" charset="utf-8" type="text/javascript"></script></div>

			<div class="wx"><img src="./cover_files/index_32.jpg" width="76" height="76"></div>

      </div>

    </div>

</div>

</div>








</body></html>